import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  input_num:number = 5;
  result:number = 0;
  constructor() { }

  ngOnInit() {
  }

  getSquare() {
   
   this.result = this.input_num * this.input_num;
  }

}
