import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-httpclientcomponent',
  templateUrl: './httpclientcomponent.component.html',
  styleUrls: ['./httpclientcomponent.component.css']
})
export class HttpclientcomponentComponent implements OnInit {

  userdata = { 
                  "data" : {"id":2,"first_name":"Janet","last_name":"Weaver","avatar":"https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg"},
                  "key2" : "abc",
                  "key3" : [1, 2, 3]
                };
  msg_of_god = {};
  ajax_response = {};
  fruits = ["mango","banana","orange","grapes"];
  employee = [
              {"name":"priyanka", "age":25, "city":"poladpur"},
              {"name":"shailesh", "age":92,"city":"nagpur"},
              {"name":"tejas", "age":18 , "city":"kanjurmage"}
  ]          
                
  constructor(private httpClient: HttpClient) {
  }

  setData(res_data) {
    this.ajax_response = res_data.data;
    console.log(this.ajax_response)
  }

  ngOnInit() {

    this.httpClient.get(`https://reqres.in/api/users`)
    .subscribe((data) =>{
      this.setData(data)
    })

     this.httpClient.get(`http://localhost/angularjs/componentdemo/get_square.php`)
    .subscribe((data) =>{
      this.msg_of_god = data;
    })
  }

}
