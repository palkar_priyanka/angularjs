import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';



import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { HttpclientcomponentComponent } from './httpclientcomponent/httpclientcomponent.component';

const appRoutes :Routes = [
                          { path : 'home', component : HomeComponent},
                          { path : 'contact', component : ContactComponent},
                          { path : 'about', component : AboutComponent},
                        ]


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    AboutComponent,
    HttpclientcomponentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {enableTracing:true}),
    HttpClientModule,
    FormsModule
  ],                                                                                                                                                                         
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
