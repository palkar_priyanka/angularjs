import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'


import { AppComponent } from './app.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { HomeComponent } from './home/home.component';

const shailesh:Routes = [
                      {path : 'contact',component: ContactusComponent },
                      {path : 'home', component : HomeComponent},
                      {path : '**' , component : PagenotfoundComponent },
                    ]

@NgModule({
  declarations: [
    AppComponent,
    ContactusComponent,
    PagenotfoundComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(shailesh,{enableTracing:true})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
